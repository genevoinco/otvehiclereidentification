!git clone https://GenEvoINC@bitbucket.org/GenEvoINC/otreidentification.git
%cd otreidentification/data
!tar -xvf duke.tar.gz >> data.txt
!tar -xvf market1501.tar.gz >> data.txt
%cd ..
!mkdir logs/duke
!pip3 install metric-learn
!pip3 install torch==1.4.0 torchvision==0.5.0
!pip install POT
!pip install scipy==1.2.0
!python3 -W ignore run.py --source_dataset market1501 --target_dataset duke --logs_dir logs/duke --EF 10 --init 0.9 --loss ExLoss --fea 2048 -m 0.5 -e 70 -s 55 -b 16 --lamda 0.8