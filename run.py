from my_reid.eug import *
from my_reid import datasets
from my_reid import models
import numpy as np
import torch
import argparse
import os

from my_reid.utils.logging import Logger
import os.path as osp
import sys
from torch.backends import cudnn
from my_reid.utils.serialization import load_checkpoint
from torch import nn
import time
import pickle


def resume(args):
    import re
    pattern=re.compile(r'step_(\d+)\.ckpt')
    start_step = -1
    ckpt_file = ""

    # find start step
    files = os.listdir(args.logs_dir)
    files.sort()
    for filename in files:
        try:
            iter_ = int(pattern.search(filename).groups()[0])
            print(iter_)
            if iter_ > start_step:
                start_step = iter_
                ckpt_file = osp.join(args.logs_dir, filename)
        except:
            continue

    # if need resume
    if start_step >= 0:
        print("continued from iter step", start_step)
    else:
        print("resume failed", start_step, files)
    return start_step, ckpt_file





def main(args):
    cudnn.benchmark = True
    cudnn.enabled = True
    save_path = args.logs_dir
    total_step = 100//args.EF + 1
    sys.stdout = Logger(osp.join(args.logs_dir, 'log'+ str(args.EF)+ time.strftime(".%m_%d_%H:%M:%S") + '.txt'))

    # get all the labeled and unlabeled data for training

    #dataset_source = datasets.create(args.source_dataset, osp.join(args.source_dir, args.source_dataset))
    #l_data_source, u_data_source = get_init_shot_in_cam1(dataset_source, load_path="./examples/{}_init_{}.pickle".format(dataset_source.name, args.init), init=args.init)

    dataset_target = datasets.create(args.target_dataset, osp.join(args.target_dir, args.target_dataset))
    l_data_target, u_data_target = get_init_shot_in_cam1(dataset_target, load_path="./examples/{}_init_{}.pickle".format(dataset_target.name, args.init), init=args.init)

    resume_step, ckpt_file = -1, ''
    if args.resume:
        resume_step, ckpt_file = resume(args)

    '''
    # initial the EUG algorithm
    eug = EUG(batch_size=args.batch_size, num_classes=dataset_source.num_train_ids,
            dataset=dataset_source, l_data=l_data_source, u_data=u_data_target, save_path=args.logs_dir, max_frames=args.max_frames,
            embeding_fea_size=args.fea, momentum=args.momentum, lamda=args.lamda)
    # build model for Optimal Transport
    eug.build_model(loss=args.loss,init_lr=0.1)


    i=0
    while( (i+1) * args.batch_size <= min(len(l_data_source),len(l_data_target))):
        #Optimal Transport
        eug.optimalTransportTrain(l_data_source,l_data_target,i,dataset_target,dataset_source);
        i = i + 1;
    eug.evaluate(dataset_target.query, dataset_target.gallery);
    eug.save_model();'''

    # initial the EUG algorithm
    eug = EUG(batch_size=args.batch_size, num_classes=dataset_target.num_train_ids,
            dataset=dataset_target, l_data=l_data_target, u_data=u_data_target, save_path=args.logs_dir, max_frames=args.max_frames,
            embeding_fea_size=args.fea, momentum=args.momentum, lamda=args.lamda);
    new_train_data = l_data_target;
    unselected_data = u_data_target;
    if args.resume:
        eug.resume(ckpt_file, resume_step)
    for step in range(total_step):
        eug.train(new_train_data, unselected_data, step,dataset_target.query, dataset_target.gallery, loss=args.loss, epochs=args.epochs, step_size=args.step_size, init_lr=0.1, resume = args.resume)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Progressive Learning for One-Example re-ID')
    parser.add_argument('-sd', '--source_dataset', type=str, default='mars',
                        choices=datasets.names())
    parser.add_argument('-td', '--target_dataset', type=str, default='mars',choices=datasets.names())
    parser.add_argument('-b', '--batch-size', type=int, default=16)
    parser.add_argument('-f', '--fea', type=int, default=1024)
    parser.add_argument('--EF', type=int, default=10)
    working_dir = os.path.dirname(os.path.abspath(__file__))
    parser.add_argument('--target_dir', type=str, metavar='PATH',
                        default=os.path.join(working_dir,'data'))
    parser.add_argument('--source_dir', type=str, metavar='PATH',
                        default=os.path.join(working_dir,'data'))
    parser.add_argument('--logs_dir', type=str, metavar='PATH',
                        default=os.path.join(working_dir,'logs'))
    parser.add_argument('--resume', type=str, default=None)
    parser.add_argument('--max_frames', type=int, default=900)
    parser.add_argument('--loss', type=str, default='ExLoss', choices=['CrossEntropyLoss', 'ExLoss'])
    parser.add_argument('--init', type=float, default=-1)
    parser.add_argument('-m', '--momentum', type=float, default=0.5)
    parser.add_argument('-e', '--epochs', type=int, default=70)
    parser.add_argument('-s', '--step_size', type=int, default=55)
    parser.add_argument('--lamda', type=float, default=0.5)
    main(parser.parse_args())
