from __future__ import print_function, absolute_import
import time
import torch.nn.functional as F
import torch
from torch import nn
from torch.autograd import Variable
import ot
from scipy.spatial.distance import cdist
from .evaluation_metrics import accuracy
from .utils.meters import AverageMeter
import numpy as np
import math


def cycle(iterable):
    while True:
        for x in iterable:
            yield x


class BaseTrainer(object):
    def __init__(self, model, criterion, lamda, fixed_layer=True):
        super(BaseTrainer, self).__init__()
        self.model = model
        self.ide_criterion = nn.CrossEntropyLoss().cuda()
        self.u_criterion = criterion
        self.fixed_layer = fixed_layer
        self.label_ratio = lamda

    def train(self, epoch,big_step, ide_data_loader, u_loader, optimizer, use_unselcted_data, print_freq=30):
        self.model.train()

        if self.fixed_layer:
            # The following code is used to keep the BN on the first three block fixed 
            fixed_bns = []
            for idx, (name, module) in enumerate(self.model.module.named_modules()):
                if name.find("layer3") != -1:
                    assert len(fixed_bns) == 22
                    break
                if name.find("bn") != -1:
                    fixed_bns.append(name)
                    module.eval() 


        batch_time = AverageMeter()
        data_time = AverageMeter()
        losses = AverageMeter()
        precisions = AverageMeter()
        end = time.time()


        u_loader = iter(cycle(u_loader))
        trainlog = TrainingLog(epoch, print_freq, len(ide_data_loader))
        ide_inputs_leftover = None
        ide_targets_leftover = None
        images_leftover = None
        miniStep = 0

        for step, ide_inputs in enumerate(ide_data_loader):
            paireUp = False
            ide_inputs, ide_targets, images = self._parse_data(ide_inputs, 'ide')
            #print(ide_targets)
            miniBatch = 16
            if paireUp:
               if type(ide_inputs_leftover) != type(None):
                    ide_inputs = torch.cat((ide_inputs, ide_inputs_leftover), 0)
                    ide_inputs_leftover = None
               if type(ide_targets_leftover) != type(None):
                    ide_targets = torch.cat((ide_targets, ide_targets_leftover), 0)
                    ide_targets_leftover = None
               if images_leftover != None:
                    images = images + images_leftover
                    images_leftover = None
               count =-2
            #print(ide_targets)
            #print(ide_inputs.shape)
            #print(ide_targets.shape)
               new_ide_target = ide_targets.clone()
               new_ide_inputs = ide_inputs.clone()
               new_images = images.copy()

               doBreak = False
               for i,id in enumerate(ide_targets):
                found = False
                if not doBreak:
                 for j,id2 in enumerate(ide_targets):
                    if j>i:
                      if id == id2:
                       if images[i].split('_')[1] != images[j].split('_')[1]:
                        found = True
                        count = count + 2
                        #if count + 1 < new_ide_target.shape[0]:
                        if count != i:
                           new_ide_target[count] =ide_targets [i];
                           new_ide_inputs[count] = ide_inputs [i];
                           new_images[count] = new_images[i]
                        new_ide_inputs[count+ 1] = ide_inputs [j];
                        new_ide_target[count+ 1] = ide_targets[i];
                        new_images[count + 1] = new_images[j]
                        '''else:
                         new_ide_target = torch.cat((new_ide_target,ide_targets [i]),0);
                         new_ide_inputs = torch.cat((new_ide_inputs, ide_inputs[i]), 0);
                         new_ide_inputs = torch.cat((new_ide_inputs, ide_inputs[j]), 0);
                         new_ide_target = torch.cat((new_ide_target, ide_targets[i]), 0);
                         new_images = new_images + new_images[i]
                         new_images = new_images + new_images[j]'''
                        #if count == miniBatch:
                        #    doBreak = True
                        break
                if not found:
                    if type(ide_targets_leftover) == type(None):
                        ide_targets_leftover = ide_targets.clone()
                        ide_targets_leftover = torch.cat((ide_targets_leftover, ide_targets_leftover), 0)
                        counter = 0
                    if type(ide_inputs_leftover) == type(None):
                        ide_inputs_leftover = ide_inputs.clone()
                        ide_inputs_leftover = torch.cat((ide_inputs_leftover,ide_inputs_leftover),0)
                    if images_leftover == None:
                        images_leftover = images.copy()
                        images_leftover = images_leftover + images_leftover

                    ide_targets_leftover[ide_targets.shape[0]+counter] = ide_targets [i]
                    ide_inputs_leftover[ide_targets.shape[0] + counter] = ide_inputs[i]
                    images_leftover[ide_targets.shape[0] + counter] = images[i]

                    counter = counter + 1


            #print(ide_inputs.shape)
            #print(ide_targets.shape)
            #print(min(16,count))


               if count>=miniBatch:
                 #print(count)
                 for m in range(0,int(count/miniBatch)):
                  data_time.update(time.time() - end)
                  ide_targets = new_ide_target[m*miniBatch:(m+1)*miniBatch]
                  ide_inputs = new_ide_inputs[m*miniBatch:(m+1)*miniBatch]

                  #print(ide_targets)
                  #print(str(m)+":  ")
                  #print(ide_targets)
                  ide_loss, ide_prec1 = self._forward(ide_inputs, ide_targets, 'ide', big_step)
                  weighted_loss = ide_loss
                  u_loss, u_prec1 = ide_loss, ide_prec1
                  optimizer.zero_grad()

                  weighted_loss.to('cuda').backward()
                  optimizer.step()
                  trainlog.update(step, weighted_loss, ide_loss, u_loss, ide_prec1, u_prec1, ide_targets)
                  #miniStep = miniStep + 1
               else:
                   counter = counter + count
                   ide_targets_leftover = torch.cat((ide_targets_leftover,new_ide_target[0:count]),0)
                   ide_inputs_leftover = torch.cat((ide_inputs_leftover, new_ide_inputs[0:count]), 0)
                   images_leftover = images_leftover + new_images[0:count]

               ide_targets_leftover = ide_targets_leftover[ide_targets.shape[0]:ide_targets.shape[0] + counter]
               ide_inputs_leftover = ide_inputs_leftover[ide_inputs.shape[0]:ide_inputs.shape[0] + counter]
               images_leftover = images_leftover[ide_inputs.shape[0]:ide_inputs.shape[0] + counter]
            else:
                ide_loss, ide_prec1 = self._forward(ide_inputs, ide_targets, 'ide', big_step)
                weighted_loss = ide_loss
                u_loss, u_prec1 = ide_loss, ide_prec1
                optimizer.zero_grad()

                weighted_loss.to('cuda').backward()
                optimizer.step()
                trainlog.update(step, weighted_loss, ide_loss, u_loss, ide_prec1, u_prec1, ide_targets)









    def get_weighted_loss(self, ide_loss, u_loss):        
        weighted_loss = ide_loss * self.label_ratio + u_loss * (1-self.label_ratio)
        return weighted_loss 

    def _parse_data(self, inputs):
        raise NotImplementedError

    def _forward(self, inputs, targets):
        raise NotImplementedError


class Trainer(BaseTrainer):
    def _parse_data(self, inputs, mode):
        imgs, images, pids, indexs, two = inputs
        inputs = Variable(imgs, requires_grad=False)
        if mode == "u":
            targets = Variable(indexs.cuda())
        elif mode == "ide":
            targets = Variable(pids.cuda())  
        else:
            raise KeyError       
        return inputs, targets, images


    def _forward(self, inputs, targets, mode, step):
        ide_preds, u_feats = self.model(inputs)
        '''print(inputs.shape)'''
        #print(ide_preds)
        '''print(u_feats.shape)
print(max(targets.data).item())'''
        if mode == "ide":
            # id predictions
            #ide_loss = F.cross_entropy(ide_preds, targets)
            gs_batch = u_feats.clone()
            gt_batch = u_feats.clone()
            g_targets = targets.clone()
            k = 0
            m = 0
            for i,ide in enumerate(u_feats):
                if i%2 == 0:
                    gs_batch[k] = ide
                    g_targets[k] = targets[i]
                    k = k + 1
                else:
                    gt_batch[m] = ide
                    m = m + 1
            gs_batch = gs_batch[0:int(gs_batch.shape[0]/2)]
            gt_batch = gt_batch[int(gt_batch.shape[0]/2):int(gt_batch.shape[0])]
            g_targets = g_targets[0:int(g_targets.shape[0]/2)]

            C0 = cdist(gs_batch.detach().cpu().numpy(), gt_batch.detach().cpu().numpy(), metric='sqeuclidean')

            C1 = F.cross_entropy(ide_preds, targets)
            #C1 = 1
            C = 0.01 * C0 + 1.0 * C1.item()
            #C =  C0 + C1.item()

            gamma = ot.emd(ot.unif(gs_batch.shape[0]), ot.unif(gt_batch.shape[0]), C)
            #print(torch.tensor(gamma).to('cuda').shape)
            #print(gs_batch.shape)

            #ide_loss =  torch.mean( F.cross_entropy(ide_preds, targets) *((1/(step+1))+ torch.tensor(gamma).float().to('cuda')*(torch.norm(gs_batch-gt_batch, 2)+F.cross_entropy(ide_preds, targets)))).to('cuda')
            ide_loss =   (F.cross_entropy(ide_preds, targets) *(1/(step+1)))+ torch.tensor(gamma).float().to('cuda')
            #ide_loss = 3.14 *ide_loss
            ide_loss = torch.mean(8*torch.log(ide_loss+1))

            #print(ide_loss)
            ide_prec, = accuracy(ide_preds.data, targets.data)
            ide_prec = ide_prec[0]
            return ide_loss, ide_prec
        elif mode == 'u':
            # u predictions
            u_loss, outputs = self.u_criterion(u_feats, targets)
            u_prec, = accuracy(outputs.data, targets.data)
            u_prec = u_prec[0]
            return u_loss, u_prec
        else:
            raise KeyError


class TrainingLog():
    def __init__(self, epoch, print_freq, data_len):
        self.batch_time = AverageMeter()
        self.losses = AverageMeter()
        self.ide_losses = AverageMeter()
        self.u_losses = AverageMeter()
        self.ide_precisions = AverageMeter()
        self.u_precisions = AverageMeter()
        self.time = time.time()

        self.epoch = epoch
        self.print_freq = print_freq
        self.data_len = data_len

    def update(self, step, weighted_loss, ide_loss, u_loss, ide_prec, u_prec, targets):
        # update time
        t = time.time()
        self.batch_time.update(t - self.time)
        self.time = t

        # weighted loss
        self.losses.update(weighted_loss.item(), targets.size(0))
        self.ide_losses.update(ide_loss.item(), targets.size(0))
        self.u_losses.update(u_loss.item())
        
        # id precision
        self.ide_precisions.update(ide_prec, targets.size(0))
        self.u_precisions.update(u_prec, targets.size(0))

        if (step + 1) % self.print_freq == 0:
            print('Epoch: [{}][{}/{}]\t'
                  'Time {:.3f} ({:.3f})\t'
                  'Loss {:.3f} ({:.3f})\t'
                  'IDE_Loss {:.3f} ({:.3f})\t'
                  'ExLoss {:.3f} ({:.3f})\t'
                  'IDE_Prec {:.1%} ({:.1%})\t'
                  'ExPrec {:.1%} ({:.1%})\t'
                  .format(self.epoch, step + 1, self.data_len,
                          self.batch_time.val, self.batch_time.avg,
                          self.losses.val, self.losses.avg,
                          self.ide_losses.val, self.ide_losses.avg,
                          self.u_losses.val, self.u_losses.avg,
                          self.ide_precisions.val, self.ide_precisions.avg,
                          self.u_precisions.val, self.u_precisions.avg))     
