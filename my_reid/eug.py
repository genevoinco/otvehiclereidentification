import torch
from torch import nn
from . import models
from .trainers import Trainer
from .evaluators import extract_features, Evaluator
from .dist_metric import DistanceMetric
import numpy as np
from collections import OrderedDict
import os.path as osp
import pickle
from .utils.serialization import load_checkpoint
from .utils.data import transforms as T
from torch.utils.data import DataLoader
from .utils.data.preprocessor import Preprocessor
import random
from scipy.spatial.distance import cdist
from .exclusive_loss import ExLoss
import copy
import ot

class EUG():
    def __init__(self, batch_size, num_classes, dataset, l_data, u_data, save_path, embeding_fea_size=1024, dropout=0.5, max_frames=900, momentum=0.5, lamda=0.5):

        self.once = True
        self.tloss = 1.0
        self.model = None
        self.num_classes = num_classes
        self.save_path = save_path
        self.model_name = 'avg_pool'

        self.l_data = l_data
        self.u_data = u_data
        self.l_label = np.array([label for _,label,_,_ in l_data])
        self.u_label = np.array([label for _,label,_,_ in u_data])
        self.data_dir = dataset.images_dir
        self.is_video = dataset.is_video

        self.batch_size = batch_size
        self.data_height = 256
        self.data_width = 256
        self.data_workers = 6


        self.dropout = dropout
        self.max_frames = max_frames
        self.embeding_fea_size = embeding_fea_size
        self.train_momentum = momentum

        self.lamda = lamda

        if self.is_video:
            self.eval_bs = 1
            self.fixed_layer = True
            self.frames_per_video = 16
        else:
            self.eval_bs = 256
            self.fixed_layer = False
            self.frames_per_video = 1

    def get_dataloader(self, dataset, training=False, is_ulabeled=False) :
        normalizer = T.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])

        if training:
            transformer = T.Compose([
                T.RandomSizedRectCrop(self.data_height, self.data_width),
                T.RandomHorizontalFlip(),
                T.ToTensor(),
                normalizer,
            ])
            batch_size = self.batch_size
        else:
            transformer = T.Compose([
                T.RectScale(self.data_height, self.data_width),
                T.ToTensor(),
                normalizer,
            ])
            batch_size = self.eval_bs

        data_loader = DataLoader(
            Preprocessor(dataset, root=self.data_dir, num_samples=self.frames_per_video,
                         transform=transformer, is_training=training, max_frames=self.max_frames),
            batch_size=16, num_workers=self.data_workers,
            shuffle=training, pin_memory=True, drop_last=training)

        current_status = "Training" if training else "Test"
        print("create dataloader for {} with batch_size {}".format(current_status, batch_size))
        return data_loader



    def train(self, train_data, unselected_data, step,query, gallery, loss, epochs=70, step_size=55, init_lr=0.1, dropout=0.5, resume = False ):
        if loss in ["CrossEntropyLoss", 'ExLoss']:
            return self.softmax_train(train_data, unselected_data, step,query, gallery, epochs, step_size, init_lr, dropout, loss,resume)
        else:
            print("{} loss not Found".format(loss))
            raise KeyError

    def save_model(self):
        ckpt_file = osp.join(self.save_path,  "step_0.ckpt")
        torch.save(self.model.state_dict(), ckpt_file)
    def build_model(self,loss,init_lr):
        """ create model """
        model = models.create(self.model_name, dropout=self.dropout, num_classes=self.num_classes,
                              embeding_fea_size=self.embeding_fea_size, classifier = loss, fixed_layer=self.fixed_layer)

        model = nn.DataParallel(model).cuda()

        # the base parameters for the backbone (e.g. ResNet50)
        base_param_ids = set(map(id, model.module.CNN.base.parameters()))
        base_params_need_for_grad = filter(lambda p: p.requires_grad, model.module.CNN.base.parameters())
        new_params = [p for p in model.parameters() if id(p) not in base_param_ids]

        # set the learning rate for backbone to be 0.1 times
        param_groups = [
            {'params': base_params_need_for_grad, 'lr_mult': 0.1},
            {'params': new_params, 'lr_mult': 1.0}]

        self.optimizer = torch.optim.SGD(param_groups, lr=init_lr, momentum=self.train_momentum, weight_decay = 5e-4, nesterov=True)

        # update the computed gamma
        self.model = model
        
    def optimalTransportTrain(self, dataset_source_laebeled,dataset_target_laebeled,i,dataset_target,dataset_source):
        # combine source_set and target_set

        #combined_set = copy.deepcopy(l_data_source)
        method = 'emd'# or sinkhorn
        g_metric = 'no'#orginal
        alpha = 0.01
        tloss = self.tloss
        # concat of source and target samples and prediction
        self.data_dir = dataset_source.images_dir
        self.is_video = dataset_source.is_video
        source_features,labels = self.get_feature(dataset_source_laebeled)# images, pid, camid, videoid = self.dataset[index]
        self.data_dir = dataset_target.images_dir
        self.is_video = dataset_target.is_video
        target_features,_ = self.get_feature(dataset_target_laebeled)  # images, pid, camid, videoid = self.dataset[index]
        modelpred = np.vstack((source_features[i:(i + 1) * self.batch_size], target_features[i:(i + 1) * self.batch_size]));
        s = modelpred.shape # (32,2048)

        #print(s)
        # intermediate features
        gs_batch = modelpred[0:self.batch_size]
        #print(gs_batch.shape)
        gt_batch = modelpred[self.batch_size: 32]
        #print(gt_batch.shape)
        # softmax prediction of target samples
        ft_pred = gs_batch
        if g_metric=='orginal':
            # compution distance metric
            if len(s) == 3:  # when the input is image, convert into 2D matrix
                C0 = cdist(modelpred.reshape(-1, s[1] * s[2]), modelpred.reshape(-1,
                                                                               s[1] * s[2]), metric='sqeuclidean')
            elif len(s) == 4:
                C0 = cdist(modelpred.reshape(-1, s[1] * s[2] * s[3]), modelpred.reshape(-1,
                                                                                s[1] * s[2] * s[3]),metric='sqeuclidean')
        else:
            # distance computation between source and target
            C0 = cdist(gs_batch, gt_batch, metric='sqeuclidean')

       #  if i==0:
       #      scale = np.max(C0)
       #  C0/=scale
        ys = []
        for y in labels:
            ys.append(int(y[1]))
        from keras.utils.np_utils import to_categorical
        ys = to_categorical(ys)
        ys = ys[i:(i + 1) * self.batch_size]
        print("ys= "+str(ys.shape))
        print("ft_pred= " + str(ft_pred.shape))
        C1 = cdist(ys, ys, metric='sqeuclidean')

        C = alpha*C0+tloss*C1

        # transportation metric

        if method == 'emd':
             gamma=ot.emd(ot.unif(gs_batch.shape[0]),ot.unif(gt_batch.shape[0]),C)
        #elif method =='sinkhorn':
        #     gamma=ot.sinkhorn(ot.unif(gs_batch.shape[0]),ot.unif(gt_batch.shape[0]),C,reg)



    def softmax_train(self, train_data, unselected_data, step,query, gallery, epochs, step_size, init_lr, dropout, loss , resume = False):

        """ create model and dataloader """
        model = models.create(self.model_name, dropout=self.dropout, num_classes=self.num_classes,
                              embeding_fea_size=self.embeding_fea_size, classifier = loss, fixed_layer=self.fixed_layer)

        model = nn.DataParallel(model).cuda()
        if resume and self.once:
            self.once = False;
            param = self.model.state_dict()
            model.load_state_dict(param)
        # the base parameters for the backbone (e.g. ResNet50)
        base_param_ids = set(map(id, model.module.CNN.base.parameters()))
        base_params_need_for_grad = filter(lambda p: p.requires_grad, model.module.CNN.base.parameters())
        new_params = [p for p in model.parameters() if id(p) not in base_param_ids]

        # set the learning rate for backbone to be 0.1 times
        param_groups = [
            {'params': base_params_need_for_grad, 'lr_mult': 0.1},
            {'params': new_params, 'lr_mult': 1.0}]


        exclusive_criterion = ExLoss(self.embeding_fea_size, len(unselected_data) , t=10).cuda()

        optimizer = torch.optim.SGD(param_groups, lr=init_lr, momentum=self.train_momentum, weight_decay = 5e-4, nesterov=True)

        # change the learning rate by step
        def adjust_lr(epoch, step_size):

            use_unselcted_data = True
            lr = init_lr / (10 ** (epoch // step_size))
            for g in optimizer.param_groups:
                g['lr'] = lr * g.get('lr_mult', 1)
            if epoch >= step_size:
                use_unselcted_data = False
                # print("Epoch {}, CE loss, current lr {}".format(epoch, lr))
            return use_unselcted_data


        s_dataloader = self.get_dataloader(train_data, training=False, is_ulabeled=False)
        u_dataloader = self.get_dataloader(unselected_data, training=True, is_ulabeled=True)


        """ main training process """

        trainer = Trainer(model, exclusive_criterion, fixed_layer=self.fixed_layer, lamda = self.lamda)
        for epoch in range(epochs):
            use_unselcted_data = adjust_lr(epoch, step_size)
            self.model = model
            #self.evaluate(query, gallery)
            trainer.train(epoch,step, s_dataloader, u_dataloader, optimizer, use_unselcted_data, print_freq=len(s_dataloader)//2)
            if epoch % 2 == 0 and epoch>0:
                ckpt_file = osp.join(self.save_path, "step_{}.ckpt".format(step))
                torch.save(model.state_dict(), ckpt_file)
                self.model = model
                self.evaluate(query, gallery)

        #ckpt_file = osp.join(self.save_path,  "step_{}.ckpt".format(step))
        #torch.save(model.state_dict(), ckpt_file)

    def get_feature(self, dataset):
        dataloader = self.get_dataloader(dataset, training=False)
        features,ys = extract_features(self.model, dataloader)
        features = np.array([logit.numpy() for logit in features.values()])
        return features,ys

    def estimate_label(self):

        # extract feature
        u_feas = self.get_feature(self.u_data)
        l_feas = self.get_feature(self.l_data)
        print("u_features", u_feas.shape, "l_features", l_feas.shape)

        scores = np.zeros((u_feas.shape[0]))
        labels = np.zeros((u_feas.shape[0]))

        num_correct_pred = 0
        for idx, u_fea in enumerate(u_feas):
            diffs = l_feas - u_fea
            dist = np.linalg.norm(diffs,axis=1)
            index_min = np.argmin(dist)
            scores[idx] = - dist[index_min]  # "- dist" : more dist means less score
            labels[idx] = self.l_label[index_min] # take the nearest labled neighbor as the prediction label

            # count the correct number of Nearest Neighbor prediction
            if self.u_label[idx] == labels[idx]:
                num_correct_pred +=1

        print("Label predictions on all the unlabeled data: {} of {} is correct, accuracy = {:0.3f}".format(
            num_correct_pred, u_feas.shape[0], num_correct_pred/u_feas.shape[0]))

        return labels, scores



    def select_top_data(self, pred_score, nums_to_select):
        v = np.zeros(len(pred_score))
        index = np.argsort(-pred_score)
        for i in range(nums_to_select):
            v[index[i]] = 1
        return v.astype('bool')



    def generate_new_train_data(self, sel_idx, pred_y):
        """ generate the next training data """

        seletcted_data = []
        unselected_data = []
        correct, total = 0, 0
        for i, flag in enumerate(sel_idx):
            if flag: # if selected
                seletcted_data.append([self.u_data[i][0], int(pred_y[i]), self.u_data[i][2], self.u_data[i][3]])
                total += 1
                if self.u_label[i] == int(pred_y[i]):
                    correct += 1
            else:
                unselected_data.append(self.u_data[i])
        acc = correct / total

        new_train_data = self.l_data + seletcted_data
        print("selected pseudo-labeled data: {} of {} is correct, accuracy: {:0.4f}  new train data: {}".format(
                correct, len(seletcted_data), acc, len(new_train_data)))
        print("Unselected Data:{}".format(len(unselected_data)))

        return new_train_data, unselected_data

    def resume(self, ckpt_file, step):
        print("continued from step", step)
        model = models.create(self.model_name, dropout=self.dropout, num_classes=self.num_classes, is_output_feature = True)
        self.model = nn.DataParallel(model).cuda()
        self.model.load_state_dict(load_checkpoint(ckpt_file))

    def evaluate(self, query, gallery):
        test_loader = self.get_dataloader(list(set(query) | set(gallery)), training = False)
        param = self.model.state_dict()
        del self.model
        model = models.create(self.model_name, dropout=self.dropout, num_classes=self.num_classes, is_output_feature = True)
        self.model = nn.DataParallel(model).cuda()
        self.model.load_state_dict(param)
        evaluator = Evaluator(self.model)
        evaluator.evaluate(test_loader, query, gallery)



"""
    Get init split for the input dataset.
"""
def get_init_shot_in_cam1(dataset, load_path, init, seed=0):

    init_str = "one-shot" if init == -1 else "semi {}".format(init)

    np.random.seed(seed)
    random.seed(seed)

    # if previous split exists, load it and return
    if osp.exists(load_path):
        with open(load_path, "rb") as fp:
            dataset = pickle.load(fp)
            label_dataset = dataset["label set"]
            unlabel_dataset = dataset["unlabel set"]

        print("  labeled  |   N/A | {:8d}".format(len(label_dataset)))
        print("  unlabel  |   N/A | {:8d}".format(len(unlabel_dataset)))
        print("\nLoad one-shot split from", load_path)
        print(init_str + "\n")
        return label_dataset, unlabel_dataset

    label_dataset = []
    unlabel_dataset = []

    if  init_str == "one-shot":
        # dataset indexed by [pid, cam]
        dataset_in_pid_cam = [[[] for _ in range(dataset.num_cams)] for _ in range(dataset.num_train_ids) ]
        for index, (images, pid, camid, videoid) in enumerate(dataset.train):
            dataset_in_pid_cam[pid][camid].append([images, pid, camid, videoid])

        # generate the labeled dataset by randomly selecting a tracklet from the first camera for each identity
        for pid, cams_data  in enumerate(dataset_in_pid_cam):
            for camid, videos in enumerate(cams_data):
                if len(videos) != 0:
                    selected_video = random.choice(videos)
                    break
            label_dataset.append(selected_video)
        assert len(label_dataset) == dataset.num_train_ids
        labeled_videoIDs =[vid for _, (_,_,_, vid) in enumerate(label_dataset)]

    else:
        # dataset indexed by [pid]
        dataset_in_pid = [ [] for _ in range(dataset.num_train_ids) ]
        old_pid = -1
        usedIds = []
        old_labels = []
        for i, (imgs, id, mid, vidid) in enumerate(dataset.train):
            #if vidid in usedIds:
            #    continue
            #else:
            couple = []
            couple.append([imgs, id, mid, vidid])
            old_labels.append(couple)
            #usedIds.append(vidid)
            for index in range(i, len(dataset.train)):
              #if dataset.train[index][3] in usedIds:
              #  continue
              if id == dataset.train[index][1] and mid != dataset.train[index][2]:
                  couple.append(dataset.train[index])
                  old_labels.append(couple)
                  break

        random.shuffle(old_labels)
        for e in old_labels:
          if len(e) == 2:
            label_dataset.append(e[0])
            label_dataset.append(e[1])



        '''for pid, pid_data  in enumerate(dataset_in_pid):
            k = int(np.ceil(len(pid_data) * init))  # random sample ratio
            selected_video = random.sample(pid_data, k)
            label_dataset.extend(selected_video)'''

        #labeled_videoIDs =[vid for _, (_,_,_, vid) in enumerate(label_dataset)]

    '''# generate unlabeled set
    for (imgs, pid, camid, videoid) in dataset.train:
        if videoid not in labeled_videoIDs:
            unlabel_dataset.append([imgs, pid, camid, videoid])'''
    #label_dataset = random.sample(label_dataset, len(label_dataset)).
    unlabel_dataset.append(label_dataset[0])
    with open(load_path, "wb") as fp:
        pickle.dump({"label set":label_dataset, "unlabel set":unlabel_dataset}, fp)


    print("  labeled    | N/A | {:8d}".format(len(label_dataset)))
    print("  unlabeled  | N/A | {:8d}".format(len(unlabel_dataset)))
    print("\nCreate new {} split and save it to {}".format(init_str, load_path))
    return label_dataset, unlabel_dataset
